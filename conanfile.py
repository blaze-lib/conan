from conans import ConanFile


class BlazeConan(ConanFile):
    name = "blaze"
    version = "3.4"
    url = "https://bitbucket.org/blaze-lib/conan/"
    homepage = "https://bitbucket.org/blaze-lib/blaze"
    license = "New (Revised) BSD license"
    description = "open-source, high-performance C++ math library for dense and sparse arithmetic"
    # No settings/options are necessary, this is header only
    no_copy_source = True

    def source(self):
        self.run("git clone https://bitbucket.org/blaze-lib/blaze")
        self.run("cd blaze && git checkout v%s" % self.version)

    def package(self):
        self.copy("*.h", src="blaze", dst="include")
